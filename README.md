SQL Sidecar SDK
=========================
Cliente de la herramienta sql-sidecar desarrollado en NodeJS cuyo propósito es servir de apoyo en las tareas de actualizaciones de versiones de bases de datos SQl.  

## Source code

La implementación se realiza en NodeJS.
El código fuente se encuentra en la carpeta `src`.

## Puesta en marcha

### Configuración

La librería recibe una configuración de la siguiente forma:

SQLSidecarSettings
```json
{
  "Host": "http://localhost:8080",
  "Version": "1.0.0",
  "Timeout": 300
}
```

__Host__: URL de acceso al API de sql-sidecar.  
__Version__: Versión destino a la que se quiere actualizar la base de datos.  
__Timeout__: Timeout a utilizar en llamadas HTTP, en segundos.  


### Uso  

La librería está disponible para su uso mediante callbacks o promesas / async/await. Se utiliza de la siguiente forma:

#### Callbacks  

```js
const SQLSidecarSettings = {
    "Host": "http://localhost:8080",
    "Version": "1.0.0",
    "Timeout": 300
};
const updateWorker = new UpdateWorker(SQLSidecarSettings);
updateWorker.doUpdateTask((err, result) => {
    if(err || result === false) {
        //there was an error
    } else {
        //update was ok
    }
});
```

#### Promesas y async/await  

```js
const SQLSidecarSettings = {
    "Host": "http://localhost:8080",
    "Version": "1.0.0",
    "Timeout": 300
};
const updateWorker = new UpdateWorker(SQLSidecarSettings);
try {
	const result = await updateWorker.doUpdateTask();
    if(!result) //there was an error
} catch(err) {
	//there was an error
}
```
  
## Contributing & License

solusoft  

Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)  