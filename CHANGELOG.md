# CHANGELOG

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/).
  
## [2.1.0] - 2020-02-12  
  
### Added  
  
* Añadida configuración para fijar el timeout en llamadas HTTP de SQL Sidecar SDK  

### Changed  

* Cambiado cliente HTTP a Axios  
  
## [2.0.0] - 2020-02-12  
  
### Changed
  
* Cambio de nombre a Node SQL Sidecar SDK  

## [1.0.0] - 2019-12-19  

### Added

* Versión inicial  