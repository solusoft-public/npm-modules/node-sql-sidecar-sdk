const unirest = require("unirest");
const axios = require('axios');

//symbols
const _sqlSidecarSettings = Symbol();

class UpdateWorker {

    constructor(sqlSidecarSettings) {
        this[_sqlSidecarSettings] = sqlSidecarSettings;
    }

    async doUpdateTask(callback) {
        let result = false;
        let error;
        try {
            const { Host, Version, Timeout = 200 } = sqlSidecarSettings;
            const body = { targetVersion: Version };
            const options = {
                timeout: Timeout * 1000,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const url = `${Host}/update/2/task`;
            const response = await axios.post(url, body, options);
            result = response.status === 200;
        } catch (err) {
            console.error("Error doing update task", err);
            error = new Error("Update process failed");
            if (!callback) throw error;
        }
        if (callback) callback(error, result);
        return result;
    }

}

module.exports = UpdateWorker;